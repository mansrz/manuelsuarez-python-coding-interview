from django.contrib.auth.models import User
from django.test import TestCase

from students.models import Student
from subjects.models import Subject


USERNAME = "test_student"
PASSWORD = "test_password"
EMAIL = "test_email@example.com"
SUBJECTS = [
    "math",
    "algorithm",
    "geometry",
    "political",
    "science",
    "science2",
    "test",
    "test2",
    "test3",
]


class StudentTestCase(TestCase):
    def setUp(self) -> None:
        self.test_user_data = {
            "username": USERNAME,
            "password": PASSWORD,
            "email": EMAIL,
        }
        self.user = User.objects.create_user(**self.test_user_data)
        self.student = Student.objects.all().first()
        return super().setUp()

    def test_post_create_user(self):
        self.assertEqual(self.user.id, self.student.student_id)

    def test_enrollment_toggle(self):
        # user enrollment_complete false by default
        self.assertEqual(self.student.enrollment_complete, False)

        for subject_name in SUBJECTS:
            Subject.objects.create(name=subject_name)
        subjects = Subject.objects.all()

        # len subjects < 7, enrollment_complete: False
        # self.student.subjects.set(subjects[:5])
        # self.student.subjects.clear()

        self.assertEqual(self.student.enrollment_complete, False)
        # # len subjects > 7, enrollment_complete: True
        # for subject in self.subjects[:6]:
        #     self.student.subjects.add(subject)

        # self.assertEqual(self.student.enrollment_complete, True)
