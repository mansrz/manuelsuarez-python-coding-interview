from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
import logging

from subjects.models import Subject

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)

class Student(models.Model):
    user = models.OneToOneField(
        User, related_name="student", on_delete=models.CASCADE
    )
    student_id = models.IntegerField()
    enrollment_complete = models.BooleanField(default=False)
    subjects = models.ManyToManyField(Subject, related_name="students")

    def __str__(self) -> str:
        return self.user.username


def student_create_after_user(sender, instance, created, **kwargs):
    logger.info((sender, instance, created))
    if created:
        new_student = Student()
        new_student.user = instance
        new_student.student_id = instance.id
        new_student.save()


post_save.connect(student_create_after_user, sender=User)
